from django.urls import path

from reviews.views import list_reviews, create_reviews, review_detail

urlpattern = [
    path("", list_reviews, name="reviews_list"),
    path("new/", create_reviews, name="create_reviews"),
    path("<int:id>/", review_detail, name="review_detail"),
]

